package pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlertsPage extends BasePage {
	
	// Constructor
	public AlertsPage(WebDriver driver) {
		super(driver);
	}
	
	// Page Variables
	String URL = "https://www.training-support.net/selenium/javascript-alerts";
	
	// Page Elements
	public By simpleAlertButton = By.cssSelector("button#simple");
	public By confirmAlertButton = By.cssSelector("button#confirm");
	public By promptAlertButton = By.cssSelector("button#prompt");
	
	// Page Methods
	public AlertsPage goToPage() {
		driver.get(this.URL);
		return this;
	}
	
	public WebElement getButton(By element) {
		WebElement button = driver.findElement(element);
		return button;
	}
	
	public void getAlertTextAndAccept() {
		Alert alert = driver.switchTo().alert();
		System.out.println(alert.getText());
		alert.accept();
	}

}
